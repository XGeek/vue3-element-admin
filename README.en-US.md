

<div align="center">
    <img src="https://img.shields.io/badge/Vue-3.4.29-brightgreen.svg"/>
    <img src="https://img.shields.io/badge/Vite-5.3.1-green.svg"/>
    <img src="https://img.shields.io/badge/Element Plus-2.7.5-blue.svg"/>
    <img src="https://img.shields.io/badge/license-MIT-green.svg"/>
    <a href="https://gitcode.com/youlai" target="_blank">
        <img src="https://img.shields.io/badge/Author-Youlai Open Source Organization-orange.svg"/>
    </a>
    <div align="center"> 中文 | <a href="./README.en-US.md">English</a></div>
</div>

![](https://foruda.gitee.com/images/1708618984641188532/a7cca095_716974.png "rainbow.png")

<div align="center">
  <a target="_blank" href="http://vue3.youlai.tech">👀 Online Preview</a> |  <a target="_blank" href="https://juejin.cn/post/7228990409909108793">📖 Read Documentation</a>  
</div>

## Project Introduction

[vue3-element-admin](https://gitcode.com/youlai/vue3-element-admin) is a free and open-source admin dashboard template built with Vue3, Vite5, TypeScript5, Element-Plus, Pinia, and other mainstream technologies (with a corresponding [backend source code](https://gitcode.com/youlai/youlai-boot)).

## Project Features

- **Simple and Easy to Use**: An upgraded Vue3 version based on vue-element-admin, with no excessive encapsulation, easy to get started.
- **Data Interaction**: Supports both local `Mock` and online APIs, with a corresponding [Java backend source code](https://gitcode.com/youlai/youlai-boot) and [online API documentation](https://www.apifox.cn/apidoc/shared-195e783f-4d85-4235-a038-eec696de4ea5).
- **Permission Management**: Comprehensive permission system features including users, roles, menus, dictionaries, and departments.
- **Infrastructure**: Dynamic routing, button permissions, internationalization, code standards, Git commit standards, commonly used component encapsulation.
- **Continuous Updates**: The project is continuously open-source and updated in real-time with tools and dependencies.

## Project Preview

![Light Mode](https://foruda.gitee.com/images/1709651876583793739/0ba1ee1c_716974.png)

![Dark Mode](https://foruda.gitee.com/images/1709651875494206224/2a2b0b53_716974.png)

![API Documentation](https://foruda.gitee.com/images/1687755822857820115/96054330_716974.png)

## Project Links

| Project | GitCode                                                        | Github                                                       | 
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ | 
| Frontend | [vue3-element-admin](https://gitcode.com/youlai/vue3-element-admin) | [vue3-element-admin](https://github.com/youlaitech/vue3-element-admin) |
| Backend | [youlai-boot](https://gitcode.com/youlai/youlai-boot)       | [youlai-boot](https://github.com/haoxianrui/youlai-boot.git) |

## Environment Setup

| Environment             | Name and Version                                               | Download Link                                               |
| -------------------- | :----------------------------------------------------------- | ------------------------------------------------------------ |
| **Development Tool**         | VSCode    | [Download](https://code.visualstudio.com/Download)           |
| **Runtime Environment**         | Node ≥18 (version 20.6.0 not supported)    | [Download](http://nodejs.cn/download)                        |

## Project Start

```bash
# Clone the code
git clone https://gitcode.com/youlai/vue3-element-admin.git

# Switch to the directory
cd vue3-element-admin

# Install pnpm
npm install pnpm -g

# Install dependencies
pnpm install

# Start the project
pnpm run dev
```

## Project Deployment

```bash
# Build the project
pnpm run build

# Upload files to the remote server
Copy the files generated in the `dist` directory to the `/usr/share/nginx/html` directory

# nginx.config configuration
server {
    listen     80;
    server_name  localhost;
    location / {
        root /usr/share/nginx/html;
        index index.html index.htm;
    }
    # Reverse proxy configuration
    location /prod-api/ {
        # Replace vapi.youlai.tech with the backend API address, keep the trailing slash /
        proxy_pass http://vapi.youlai.tech/; 
    }
}
```

## Local Mock

The project supports both online and local Mock APIs. By default, it uses online APIs. To switch to Mock APIs, set the `VITE_MOCK_DEV_SERVER` variable in the `.env.development` file to `true`.

## Backend API

> If you have a Java development background, follow the steps below to convert online APIs to local backend APIs, creating an enterprise-level frontend-backend separation development environment, leading you to full-stack development.

1. Obtain the backend [youlai-boot](https://gitcode.com/youlai/youlai-boot.git) source code developed in `Java` and `SpringBoot`.
2. Complete local startup as per the backend project instructions in [README.md](https://gitcode.com/youlai/youlai-boot#%E9%A1%B9%E7%9B%AE%E8%BF%90%E8%A1%8C).
3. Change the value of `VITE_APP_API_URL` in the `.env.development` file from http://vapi.youlai.tech to http://localhost:8989.

## Notes

- **Auto-import Plugin Default Disabled**

  The component type declarations of the template project have been automatically generated. If new components are added and used, enable auto-generation as shown. After auto-generation, set it back to `false` to avoid conflicts.

  ![](https://foruda.gitee.com/images/1687755823137387608/412ea803_716974.png)

- **Blank Page on Project Startup in Browser**

  Please upgrade the browser. Older browser versions may not support some new JavaScript syntax like optional chaining `?.`.

- **Updating and Upgrading Project Repository**

  After updating and upgrading the project repository, it is recommended to run `pnpm install` to install updated dependencies before starting.

- **Component, Function, and Reference Errors**

  Try restarting VSCode.

- **Other Issues**

  For other issues or suggestions, please submit an [ISSUE](https://gitcode.com/youlai/vue3-element-admin/issues/new).

## Project Documentation

- [Building an Admin System from Scratch with Vue3 + Vite + TypeScript + Element-Plus](https://blog.csdn.net/u013737132/article/details/130191394)
- [ESLint+Prettier+Stylelint+EditorConfig to Enforce and Unify Frontend Code Standards](https://blog.csdn.net/u013737132/article/details/130190788)
- [Configuring Git Commit Standards with Husky + Lint-staged + Commitlint + Commitizen + cz-git](https://blog.csdn.net/u013737132/article/details/130191363)

## Commit Standards

Run `pnpm run commit` to initiate an interactive git commit, and follow the prompts to complete the information entry and selection.

![](https://foruda.gitee.com/images/1687755823165218215/c1705416_716974.png)

## Project Statistics

![Alt](https://repobeats.axiom.co/api/embed/aa7cca3d6fa9c308fc659fa6e09af9a1910506c3.svg "Repobeats analytics image")

Thanks to all the contributors!

[![contributors](https://contrib.rocks/image?repo=youlaitech/vue3-element-admin)](https://github.com/youlaitech/vue3-element-admin/graphs/contributors)

## Discussion Group🚀

> **Follow the "Youlai Technology" public account to get the discussion group QR code.**
>
> If the QR code for the discussion group expires, please add WeChat (haoxianrui) and note "Frontend", "Backend", or "Full Stack" to get the latest QR code.
>
> To ensure the quality of the discussion group and prevent marketing and advertising people from joining, we have adopted this measure. Thank you for understanding!

| Public Account | Discussion Group |
|:----:|:----:|
| ![Youlai Technology Public Account QR Code](https://foruda.gitee.com/images/1687689212187063809/3c69eaee_716974.png) | ![Discussion Group QR Code](https://foruda.gitee.com/images/1687689212139273561/6a65ef69_716974.png) |

